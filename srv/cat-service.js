const cds = require("@sap/cds");
const cron = require('node-cron');
const { log } = require("@sap/hdi-deploy/lib/logger");
const {Final2,Registry} = cds.db.entities




module.exports = async function(srv){

    // crono-job
    async function executeDailyJob() {
        try {
          // Connect to the SAP HANA database using the CAP database service

          let data = '2023-07-04 08:20:28.355000000';
          // Call your stored procedures or execute your SQL statements
          await cds.run(`call "correctiveprocedure"()`);
          await cds.run(`call "whilestatement"(v_numb => ?, var_date => '${data}')`);
      
          console.log('Daily job executed successfully');
        } catch (error) {
          console.error('Error executing daily job:', error);
        }
      }
      
      cron.schedule('0 18 * * *', executeDailyJob);
 

    srv.before("CREATE","Historic" ,async function(req){
            let data = req.data;
           
           
                let dbQuery = `call "checkinsertprocedure"(v_data => '${JSON.stringify(data)}', v_out => ? , v_out2 => ?)`
                let result = await cds.run(dbQuery, {}) 
           
                if(result.V_OUT === 0) throw new Error("Check data insert , somenthing is wrong");
            

        });

    srv.after("READ","Final2" ,async function(req){
        let data = '2023-07-04 08:20:28.355000000';
        let dbQuery2 = 'call "correctiveprocedure"()'
        await cds.run(dbQuery2, {})

        let dbQuery3 = `call "whilestatement"(v_numb => ?, var_date => '${data}')`
        await cds.run(dbQuery3, {}) 
    });

    srv.after("READ","Registry",async function(req){                   
        
            
            let dbQuery = 'call "transictiontest"()'
            let result = await cds.run(dbQuery, {})   
        
        
    })

                     // srv.before("READ","Final2",async function(req){
            
    //             // let data = '2023-05-24 10:49:21.000000000';

    //                     let dbQuery = `call "whilestatement"(v_numb => ?)`
    //                     await cds.run(dbQuery, {})       
    //     })


    // this.on('correctiveprocedure', async () => {
    //         try {
                
    //     let dbQuery = 'call "correctiveprocedure"()'
    //             let result = await cds.run(dbQuery, {})
    //             console.log(result)
    //             return result
    //         } catch (error) {
    //             console.error(error)
    //             return false
    //         }
    //         });



                    // this.before('transictiontest', async () => {
                    //     try {
                        
                    // let dbQuery = 'call "transictiontest"()'
                    //         let result = await cds.run(dbQuery, {})
                    //         console.log(result)
                    //         return true
                    //     } catch (error) {
                    //         console.error(error)
                    //         return false
                    //     }
                    //     })

    }


    //      Versione with queryParameters
                //     srv.before("READ","Final2",async function(req){
                        
                //         let data = req._queryOptions["\"p_date"];
                        
                                
                //                 // let dbQuery = `call "updateregistry"(var_time =>  '${data}' ,var_name => '${upID}')`
                //                 // let result = await cds.run(dbQuery, {})
                                
                //                 // let dbQuery2 = `call "finalprocedure"(var_time =>  '${data}' ,var_name => '${upID}',et_items2 => ?)`
                //                 // let result2 = await cds.run(dbQuery2, {})
                //                 // console.log(result2)

                //                 let dbQuery = `call "whilestatement"(v_date => '${data}', v_numb => ?)`
                //                 await cds.run(dbQuery, {})    
                // })

                       //Procedure di prova 
        // this.on('firstprocedure', async () => {
        //     try {
                
        //         let dbQuery = 'call "firstprocedure"(pick =>  \'2023-05-24 10:49:21.000000000\' ,et_items => ?)'
        //         let result = await cds.run(dbQuery, {})
        //         console.log(result)
        //         return result
        //     } catch (error) {
        //         console.error(error)
        //         return error
        //     }
        //     })


        // this.on('finalprocedure', async () => {
        //     try {
                
        //         let dbQuery = 'call "finalprocedure"(var_time =>  \'2023-05-24 10:49:21.000000000\' ,var_name => \'Muzi\',et_items2 => ?)'
        //         let result = await cds.run(dbQuery, {})
        //         console.log(result)
        //         return result
        //     } catch (error) {
        //         console.error(error)
        //         return error
        //     }
        //     })


        // this.on('updateregistry', async () => {
        //     try {
               
        // let dbQuery = 'call "updateregistry"(var_time =>  \'2023-05-24 10:49:21.000000000\' ,var_name => \'d78b9931-f756-48eb-9bb5-e9bd79ea0379\')'
        //         let result = await cds.run(dbQuery, {})
        //         console.log(result)
        //         return true
        //     } catch (error) {
        //         console.error(error)
        //         return false
        //     }
        //     })

            // this.on('whilestatement', async () => {
            //     try {
                   
            // let dbQuery = 'call "whilestatement"(v_date => \'2023-06-01 10:48:56.000000000\', v_numb => ?)'
            //         let result = await cds.run(dbQuery, {})
            //         console.log(result)
            //         return true
            //     } catch (error) {
            //         console.error(error)
            //         return false
            //     }
            //     })