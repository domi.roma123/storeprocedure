using storedProcedure as db from '../db/data-model';


service storedProcedureService {
      entity Registry as  projection on db.Registry;

      entity Event as  projection on db.Event;

      entity Historic as projection on db.Historic;

      entity Final as projection on db.Final;

      entity Final2 as projection on db.Final2;

      entity Status as projection on db.Status;
      

      function firstprocedure() returns String;
      function finalprocedure() returns String;
      function updateregistry() returns Boolean;
      function whilestatement() returns Boolean;
      function correctiveprocedure()  returns String;
      function transictiontest() returns Boolean;
      function checkinsertprocedure() returns Boolean;
      
      

}