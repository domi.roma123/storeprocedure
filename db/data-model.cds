namespace storedProcedure;

using
{
    cuid
}
from '@sap/cds/common';


entity Registry : cuid {
    userName: String;
    surname: String;
    userScore: Integer;
    createdDate  : Timestamp @cds.on.insert : $now;

}

entity Event : cuid {
    eventName: String;
    pointEvent: Integer;
}

entity Historic : cuid {
    user: Association to  Registry;
    Event: Association to  Event;
    reference: String default NULL;
    status: Association to one Status;
    h_time: Timestamp @cds.on.insert : $now;
}


entity Final : cuid {
   	r_userName: String;
    r_surname: String;
    r_userScore: Integer;
    r_eventName: String;
    r_pointEvent: Integer;
    r_Time: Timestamp;
}

entity Final2 : cuid {
    f_user: Association to Registry;
    f_score: Integer;
    f_Time: Timestamp;
}

entity Status : cuid{
    code: String;
}